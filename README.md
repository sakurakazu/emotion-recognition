# Emotion recognition

Demonstration of emotion recognition with Python and Keras library implementing convolution neural network (CNN). Based
on [Abhijeet Kumar's article](https://appliedmachinelearning.blog/2018/11/28/demonstration-of-facial-emotion-recognition-on-real-time-video-using-cnn-python-keras/).

## Prerequisites

I recommend use of Python 3.6. At the project root, create three folders

```bash
$ mkdir captures data models
$ cd data && mkdir cascades fer2013
```

`captures` directory is where the program will output captured images. `models` directory is where program will output
the trained model with logs. Lastly, `data` directory has two subdirectories: `cascades` holds Haar cascade which you
can get [here](https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml). I
recommend saving it with the same name, that is `haarcascade_frontalface_default.xml`. The `fer2013` subdirectory contains
FER dataset which you should get from [here](https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge/data).

Extract the dataset and you should have a project structure similar to this:

```
├── captures
├── data
│   ├── cascades
│   │   └── haarcascade_frontalface_default.xml
│   └── fer2013
│       ├── fer2013.bib
│       ├── fer2013.csv
│       └── README
├── models
├── README.md
├── src
│   ├── image-capture.py
│   └── train-model.py
```

## Training the model

Before you can run the `image-capture.py`, you need to train the model. Run `train-model.py`.

## Testing model

To test the operation, you can use the `image-capture.py`, which takes a photo with default device camera (on a laptop
that would be a face camera) and prints the prediction result in the terminal.
