import cv2
import datetime
from keras.preprocessing import image
from keras.models import load_model
import numpy as np


def apply_offsets(face_coordinates, offsets):
    """
    Apply offset (padding) to a captured face
    :param face_coordinates: Cartesian coordinates of box enclosing a recognized face
    :param offsets: width/height value of offset
    :return: bounding box with corrected coordinates
    """
    x, y, width, height = face_coordinates
    x_off, y_off = offsets
    return x - x_off, x + width + x_off, y - y_off, y + height + y_off


def load_image(image_path, grayscale=False, target_size=None):
    """
    Load image from the memory using Pil library
    :param image_path: relative path
    :param grayscale: boolean value telling us if the image is grayscaled or not
    :param target_size: parameters used to determine target size of the image, I included this just in case
    :return: converted PIL Image instance into a Numpy array
    """
    pil_image = image.load_img(image_path, grayscale, target_size)
    return image.img_to_array(pil_image)


def preprocess_input(x, v2=True):
    x = x.astype('float32')
    x = x / 255.0
    if v2:
        x = x - 0.5
        x = x * 2.0
    return x


# Initialize capture on a default device (laptop's webcamera, for instance...)
capture = cv2.VideoCapture(0)
ret, frame = capture.read()

# Prepare image name
image_name = '../captures/' + str(datetime.datetime.now()) + '.jpg'
image_modified_name = '../captures/' + str(datetime.datetime.now()) + '-modified.jpg'

# Store image in a captures directory
cv2.imwrite(image_name, frame)

# Clean up after yourself...
cv2.destroyAllWindows()
capture.release()

# Paths to cascades and model
haar_cascade = '../data/cascades/haarcascade_frontalface_default.xml'
emotion_model = '../models/_mini_EXEPTION.h5'

# Load classifier for cascade and our model
face_detection = cv2.CascadeClassifier(haar_cascade)
emotion_classifier = load_model(emotion_model, compile=False)

original_frame = cv2.imread(image_name)

# Initialize facial detection
faces = face_detection.detectMultiScale(original_frame, 1.3, 5)

# List of emotion labels IN A CORRECT ORDER FROM DATASET
emotions = ['angry', 'disgust', 'scared', 'happy', 'sad', 'surprised', 'neutral']

for face in faces:
    x1, x2, y1, y2 = apply_offsets(face, (0, 0))
    gray_face = load_image(image_name, grayscale=True)

    gray_face = cv2.resize(gray_face, (emotion_classifier.input_shape[1:3]))

    gray_face = preprocess_input(gray_face, True)
    gray_face = np.expand_dims(gray_face, 0)
    gray_face = np.expand_dims(gray_face, -1)

    # Important bits occur here
    prediction = emotion_classifier.predict(gray_face)
    emotion_label = np.argmax(prediction)
    emotion = emotions[emotion_label]

    print('Prediction: ' + str(prediction))
    print('Emotion label: ' + str(emotion_label))
    print('Recognized emotion: ' + str(emotion))
