import cv2
import numpy
import pandas
from keras import layers
from keras.layers import Activation, Conv2D
from keras.layers import BatchNormalization
from keras.layers import GlobalAveragePooling2D
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import SeparableConv2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from sklearn.model_selection import train_test_split

# Dataset file
dataset = '../data/fer2013/fer2013.csv'
image_size = (48, 48)


def load_dataset():
    """
    Load the CSV dataset and transform it into an appropriate format, meaning that we generate a 2D Numpy array
    :return: list of faces and corresponding emotion labels
    """
    faces_list = []
    width, height = 48, 48
    data = pandas.read_csv(dataset)
    pixels = data['pixels'].tolist()

    for pixelSequence in pixels:
        face = [int(pixel) for pixel in pixelSequence.split(' ')]
        face = numpy.asarray(face).reshape(width, height)
        face = cv2.resize(face.astype('uint8'), image_size)
        faces_list.append(face.astype('float32'))

    faces_list = numpy.asarray(faces_list)
    faces_list = numpy.expand_dims(faces_list, -1)

    emotions_list = pandas.get_dummies(data['emotion']).as_matrix()

    return faces_list, emotions_list


def preprocess_input(x):
    '''
    Standard preprocessing of images before using them for training
    :param x: list of faces
    :return: 2D array (faces) suitable for training
    '''
    x = x.astype('float32') / 255.0
    return (x - 0.5) * 2.0


# Load dataset and initiate preprocessing
faces, emotions = load_dataset()
faces = preprocess_input(faces)

# Split dataset into train and test subsets (80% for training, and 20% for testing) + shuffle before splitting
xtrain, xtest, ytrain, ytest = train_test_split(faces, emotions, test_size=0.2, shuffle=True)

# Parameters used during training
batch_size = 32
epochs_number = 100
input_shape = (48, 48, 1)
verbose = 1
classes_number = 7
patience = 50
base_path = '../models/'
l2_regularization = 0.01

# Generates batches of tensor image data with real-time data augmentation
data_generator = ImageDataGenerator(
    featurewise_center=False,
    featurewise_std_normalization=False,
    rotation_range=10,
    width_shift_range=0.1,
    height_shift_range=0.1,
    zoom_range=0.1,
    horizontal_flip=True
)

# Model parameters
regularization = l2(l2_regularization)

# Base
image_input = Input(input_shape)

# Temporal convolution layer
x = Conv2D(8, (3, 3), strides=(1, 1), kernel_regularizer=regularization, use_bias=False)(image_input)

# Batch normalization layer (normalize the activations of the temporal convolution layer)
x = BatchNormalization()(x)

# Use Rectified Linear Unit (relu) activation
x = Activation('relu')(x)

# Second convolution and normalization layer and another activation
x = Conv2D(8, (3, 3), strides=(1, 1), kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)

# Now we begin assembling more modules, each time incrementing the dimensionality of the Conv2D layer. This process is
# repeated 4 times, as suggested by the Xception model paper
# Module 1
residual = Conv2D(16, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
residual = BatchNormalization()(residual)

# SeperableConv2D has a lower computational cost (https://arxiv.org/abs/1801.04381)
x = SeparableConv2D(16, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = SeparableConv2D(16, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)

# We need to perform pooling in order to down-sample the input representation (reducing its dimensionality) and allowing
# for assumptions to be made about features contained in these regions. This is a recurring process for each module
x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
x = layers.add([x, residual])

# Module 2
residual = Conv2D(32, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
residual = BatchNormalization()(residual)
x = SeparableConv2D(32, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = SeparableConv2D(32, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
x = layers.add([x, residual])

# Module 3
residual = Conv2D(64, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
residual = BatchNormalization()(residual)
x = SeparableConv2D(64, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = SeparableConv2D(64, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
x = layers.add([x, residual])

# Module 4
residual = Conv2D(128, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
residual = BatchNormalization()(residual)
x = SeparableConv2D(128, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = Activation('relu')(x)
x = SeparableConv2D(128, (3, 3), padding='same', kernel_regularizer=regularization, use_bias=False)(x)
x = BatchNormalization()(x)
x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
x = layers.add([x, residual])
x = Conv2D(classes_number, (3, 3), padding='same')(x)
x = GlobalAveragePooling2D()(x)
output = Activation('softmax', name='predictions')(x)

# Add training & evaluation routines to the network
model = Model(image_input, output)

# Compile the model with Adam optimizer, use categorical crossentropy loss function (we found out it works best) and
# make sure that the model is compiled for accuracy (compiling it for speed does not significantly alter the speed of
# recognition on mobile devices)
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Print summary
model.summary()

# Train the model on data generated batch-by-batch by a Python generator, running in parallel to the model
model.fit_generator(
    data_generator.flow(xtrain, ytrain, batch_size),
    steps_per_epoch=len(xtrain) / batch_size,
    epochs=epochs_number,
    verbose=1,
    validation_data=(xtest, ytest))

# Save the model
model.save(base_path + '_mini_EXEPTION.h5')
